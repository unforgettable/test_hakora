﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_HP_die : MonoBehaviour
{
    public int health = 40;

    public void TakeDamage(int amount)
    {
        health -= amount;

        if (health <= 0)
        {
            Die();
        }
    }

    public virtual void Die()
    {
        Destroy(gameObject);
    }
}
