﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    GameObject Player;
    public float moveSpeed = 5f;
    
    private Rigidbody2D rb;

    private static List<Rigidbody2D> EnemyRBs;

    [Range(0f, 1f)]
    public float turnSpeed = .1f;

    public float repelRange = .5f;
    public float repelAmount = 1f;

    public float startMaxChaseDistance = 20f;
    private float maxChaseDistance;

    public bool isShooter = false;
    void Start()
    {
        Player = GameObject.Find("Player");
        rb = GetComponent<Rigidbody2D>();
        if (EnemyRBs == null)
        {
            EnemyRBs = new List<Rigidbody2D>();
        }

        EnemyRBs.Add(rb);
    }

    // Update is called once per frame
    void Update()
    {
        if (Player != null)      //Если игрок жив и находится на сцене
        {
            Vector2 direction = (new Vector2(Player.transform.position.x, Player.transform.position.y) - rb.position).normalized;

            Vector2 newPos;

            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
            rb.rotation = Mathf.LerpAngle(rb.rotation, angle, turnSpeed);

            newPos = MoveRegular(direction);

            rb.MovePosition(newPos);
        }
    }

    Vector2 MoveRegular(Vector2 direction)
    {
        Vector2 repelForce = Vector2.zero;
        foreach (Rigidbody2D enem in EnemyRBs)
        {
            if (enem == rb)
                continue;

            if (Vector2.Distance(enem.position, rb.position) <= repelRange)
            {
                Vector2 repelDir = (rb.position - enem.position).normalized;
                repelForce += repelDir;
            }
        }

        Vector2 newPos = transform.position + transform.up * Time.fixedDeltaTime * moveSpeed;
        newPos += repelForce * Time.fixedDeltaTime * repelAmount;

        return newPos;
    }

    private void OnDestroy()
    {
        EnemyRBs.Remove(rb);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        User_HP_Die player = collision.collider.GetComponent<User_HP_Die>();
        player.TakeDamage(10);
        Destroy(gameObject);
    }
}
