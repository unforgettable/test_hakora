﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class User_HP_Die : MonoBehaviour
{
    public float health;

    private void Start()
    {
        health = 10000;
    }
    public void TakeDamage(float amount)
    {
        health -= amount;

        if (health <= 0)
        {
            Die();
        }
    }

    public virtual void Die()
    {
        Destroy(gameObject);
    }
}
