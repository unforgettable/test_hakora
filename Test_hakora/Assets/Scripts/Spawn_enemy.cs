﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn_enemy : MonoBehaviour
{
    public float startSpawnRadius = 100f;
    private float spawnRadius;
    public GameObject spawn;
    public GameObject enemy_pr;

    [SerializeField]
    Transform ParrentEnemy;

    [SerializeField]
    private float Spawn_Rate = 1;
    private float nextSpawnTime = 1f;

    [SerializeField]
    bool Infinitely_Spawn = false;
    [SerializeField]
    int CountSpawnMob = 10;
    int StartCount = 0;


    // Start is called before the first frame update
    void Start()
    {
        ParrentEnemy = GameObject.Find("All_Enemy").transform;
    }

    void Update()
    {
        if(Infinitely_Spawn)            //Если спавнер бесконечный
            SpawnMob();
        else                            //Если спавнер не бесконечный
        {
            if (StartCount < CountSpawnMob)
            {
                if (SpawnMob())          //Если моб отспавнен
                    StartCount++;       //Увеличиваем количество отспавненных мобов
            }
            else
                Destroy(gameObject);
        }

    }
    bool SpawnMob()
    {
        spawnRadius = startSpawnRadius;
        if (Time.time >= nextSpawnTime)
        {
            SpawnEnemy(enemy_pr);
            nextSpawnTime = Time.time + Spawn_Rate;
            return true;
        }
        else return false;
    }

    void SpawnEnemy(GameObject enemyPrefab)
    {
        Vector2 spawnPos = spawn.transform.position;
        spawnPos += Random.insideUnitCircle.normalized * 10f;

        GameObject Enemy = Instantiate(enemyPrefab, spawnPos, Quaternion.identity);
        Enemy.transform.SetParent(ParrentEnemy);        //Помещение в родителя - All_Enemy
    }
}
